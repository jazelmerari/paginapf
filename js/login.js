// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCMewBdiMlpfN52u6qYQgFDCRGAfpsdhzM",
    authDomain: "proyectofmj.firebaseapp.com",
    projectId: "proyectofmj",
    storageBucket: "proyectofmj.appspot.com",
    messagingSenderId: "570499264757",
    appId: "1:570499264757:web:9fd2e15a7c345d82d595bc"
};

// Inicializar Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

// Referencias a los elementos del DOM
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("contra");
const loginButton = document.getElementById("btnEnviar");
const errorMessage = document.getElementById("error");

// Escuchar el evento de click en el botón de ingreso
loginButton.addEventListener("click", (e) => {
    e.preventDefault(); // Prevenir el comportamiento por defecto de enviar el formulario

    const email = emailInput.value;
    const password = passwordInput.value;

    // Autenticar al usuario
    signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
            // Inicio de sesión exitoso
            console.log("¡Inicio de sesión exitoso!");
            // Redireccionar al usuario a la página principal o a donde necesites
            window.location.href = '../html/menu.html';
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log(`Error ${errorCode}: ${errorMessage}`);
            // Mostrar mensaje de error al usuario
            const errors = document.querySelector('.error');
            errors.classList.add('errors')
            setTimeout(() => {
                errors.classList.remove('errors');
            }, 2000);
            document.querySelector(".error").textContent = "Intenta de nuevo";
        });
});

// Observar el estado de autenticación
onAuthStateChanged(auth, (user) => {
    if (user) {
        // Usuario está autenticado
        console.log("Usuario autenticado:", user);
        // Puedes redireccionar al usuario a otra página o cargar contenido exclusivo para usuarios autenticados
    } else {
        // Usuario no está autenticado
        console.log("Usuario no autenticado");
    }
});
