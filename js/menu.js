import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getDatabase, ref, set, onChildAdded, onChildRemoved, remove, update, onChildChanged, onValue } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';

// Configuración de Firebase
const firebaseConfig = {
    apiKey: "AIzaSyCMewBdiMlpfN52u6qYQgFDCRGAfpsdhzM",
    authDomain: "proyectofmj.firebaseapp.com",
    databaseURL: "https://proyectofmj-default-rtdb.firebaseio.com",
    projectId: "proyectofmj",
    storageBucket: "proyectofmj.appspot.com",
    messagingSenderId: "570499264757",
    appId: "1:570499264757:web:9fd2e15a7c345d82d595bc"
};

const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage(app);

function uploadImage(file, callback) {
    const sRef = storageRef(storage, 'imagen/' + Date.now() + '-' + file.name);
    const uploadTask = uploadBytesResumable(sRef, file);

    uploadTask.on('state_changed',
        (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
        },
        (error) => {
            console.error('Error uploading image:', error);
        },
        () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                callback(downloadURL);
            });
        }
    );
}

function resetForm() {
    document.getElementById('nombre').value = '';
    document.getElementById('precio').value = '';
    document.getElementById('descripcion').value = '';
    document.getElementById('cantidad').value = '';
    document.getElementById('productImage').value = '';
    document.getElementById('productId').value = ''; // Limpiar el campo oculto del producto
}

document.getElementById('productForm').addEventListener('submit', function (event) {
    event.preventDefault();

    const nombre = document.getElementById('nombre').value;
    const precio = document.getElementById('precio').value;
    const descripcion = document.getElementById('descripcion').value;
    const cantidad = document.getElementById('cantidad').value;
    const imageFile = document.getElementById('productImage').files[0];
    const existingKey = document.getElementById('productId').value;

    saveProduct(nombre, precio, descripcion, cantidad, imageFile, existingKey);

    resetForm();  // Limpiar el formulario después de guardar el producto
});

function saveProduct(nombre, precio, descripcion, cantidad, imageFile, existingKey) {
    if (!nombre || !precio || !descripcion || !cantidad) {
        console.error("Datos incompletos, no se guardará el producto");
        return;
    }

    function storeData(imageUrl) {
        const data = {
            nombre: nombre,
            precio: precio,
            descripcion: descripcion,
            cantidad: cantidad,
            imageUrl: imageUrl
        };

        if (existingKey) {
            // Si existingKey está presente, estamos editando un producto existente
            const productRef = ref(db, `productos/` + existingKey);
            update(productRef, data);
        } else {
            // Si existingKey no está presente, estamos creando un nuevo producto
            const newProductRef = ref(db, `productos/` + Date.now());
            set(newProductRef, data);
        }
    }

    if (imageFile) {
        uploadImage(imageFile, (imageUrl) => {
            storeData(imageUrl);
        });
    } else {
        storeData(null);
    }
}

function deleteProduct(productId) {
    const productRef = ref(db, `productos/` + productId);
    remove(productRef);
}

function updateProductInView(productId, data) {
    // Actualizar los elementos de la interfaz de usuario con el productId y los nuevos datos
}

function updateProductInTable(productId, data) {
    // Actualizar la fila en la tabla con el productId y los nuevos datos
}

function fillFormForEdit(key) {
    const productRef = ref(db, `productos/` + key);
    onValue(productRef, (snapshot) => {
        const product = snapshot.val();
        document.getElementById('nombre').value = product.nombre;
        document.getElementById('precio').value = product.precio;
        document.getElementById('descripcion').value = product.descripcion;
        document.getElementById('cantidad').value = product.cantidad;
        document.getElementById('productId').value = key;  // Guarda la clave del producto en el campo oculto
        // No puedes establecer el archivo de imagen en el input debido a políticas de seguridad del navegador
    });
}

onChildAdded(ref(db, 'productos/'), (snapshot) => {
    const key = snapshot.key;
    const product = snapshot.val();
    insertProductInTable(key, product);
});

onChildChanged(ref(db, 'productos/'), (snapshot) => {
    const key = snapshot.key;
    const product = snapshot.val();
    const existingRow = document.getElementById(`product-${key}`);
    if (existingRow) {
        existingRow.innerHTML = `
        <td>${product.nombre}</td>
        <td>${product.precio}</td>
        <td>${product.descripcion}</td>
        <td>${product.cantidad}</td>
        <td><img src="${product.imageUrl || ''}" alt="${product.nombre}" height="50"/></td>
        <td>
          
        </td>
      `;
    }
});

onChildRemoved(ref(db, 'productos/'), (snapshot) => {
    const key = snapshot.key;
    const rowToRemove = document.getElementById(`product-${key}`);
    if (rowToRemove) {
        rowToRemove.remove();
    }
});

// La función deleteProduct ya se proporcionó previamente.
function displayProducts() {
    // Crear tabla y añadirla al DOM
    const table = document.createElement("table");
    const thead = document.createElement("thead");
    const tbody = document.createElement("tbody");
    table.appendChild(thead);
    table.appendChild(tbody);
    table.classList.add('table')
    table.classList.add('table-dark');
    document.getElementById('productList').appendChild(table);

    // Crear encabezado de la tabla
    const trHeader = document.createElement("tr");
    ["Nombre", "Precio", "Descripción", "Cantidad", "Imagen", "Acciones"].forEach(headerText => {
        const th = document.createElement("th");
        th.textContent = headerText;
        trHeader.appendChild(th);
    });
    thead.appendChild(trHeader);

    const productsRef = ref(db, 'productos');

    // Añadir productos
    onChildAdded(productsRef, snapshot => {
        console.log(`Añadiendo producto con ID ${snapshot.key}`);

        const data = snapshot.val();
        const tr = document.createElement("tr");
        tr.id = `product-${snapshot.key}`;  // Aseguramos un ID único

        // Nombre
        const tdName = document.createElement("td");
        tdName.textContent = data.nombre;
        tr.appendChild(tdName);

        // Precio
        const tdPrice = document.createElement("td");
        tdPrice.textContent = data.precio;
        tr.appendChild(tdPrice);

        // Descripción
        const tdDescription = document.createElement("td");
        tdDescription.textContent = data.descripcion;
        tr.appendChild(tdDescription);

        // Cantidad
        const tdQuantity = document.createElement("td");
        tdQuantity.textContent = data.cantidad;
        tr.appendChild(tdQuantity);

        // Imagen
        const tdImage = document.createElement("td");
        const img = document.createElement("img");
        img.src = data.imageUrl;
        img.alt = data.nombre;
        img.width = 100;
        tdImage.appendChild(img);
        tr.appendChild(tdImage);

        // Acciones
        const tdActions = document.createElement("td");
        const deleteButton = document.createElement("button");
        deleteButton.classList.add('botonE');
        deleteButton.innerText = "Eliminar";
        deleteButton.onclick = () => deleteProduct(snapshot.key);
        tdActions.appendChild(deleteButton);

        const editButton = document.createElement("button");
        editButton.classList.add('botonEdi');
        editButton.innerText = "Editar";
        editButton.onclick = () => fillFormForEdit(snapshot.key);
        tdActions.appendChild(editButton);
        tr.appendChild(tdActions);

        tbody.appendChild(tr);
    });

    // Actualizar productos
    onChildChanged(productsRef, snapshot => {
        console.log(`Actualizando producto con ID ${snapshot.key}`);
        const data = snapshot.val();
        updateProductInTable(`product-${snapshot.key}`, data);
    });

    // Eliminar productos
    onChildRemoved(productsRef, snapshot => {
        const trToRemove = document.getElementById(`product-${snapshot.key}`);
        if (trToRemove) {
            trToRemove.remove();
        }
    });
}


// Llama a la función para mostrar los productos y habilitar la edición y eliminación en tiempo real
displayProducts();
